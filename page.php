<?php
require_once('db/db.php');
$id = $id = $_GET['id'];
?>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title></title>
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="../css/style_1.css">
</head>

<body>
  <div class="tab-group">
    <a href="/test">Back</a>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="form">

          <?php
            $sql = "SELECT * FROM `page` WHERE id = $id";
            $rs = mysqli_query($conn,  $sql);

            while($row = mysqli_fetch_assoc($rs)) {
                 $data = $row;
          ?>

          <div style="color: #a0b3b0"><?php echo $data['title'] ?></div>
          <div style="color: #a0b3b0"><?php echo $data['meta'] ?></div>
          <div style="color: #a0b3b0"><?php echo $data['content'] ?></div>

          <?php } ?>

        </div>
      </div>
    </div>
  </div>

  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script  src="../js/index.js"></script>

  </body>

  </html>
