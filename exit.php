<?php
session_start();

unset($_COOKIE['user_id']);
unset($_COOKIE['username']);

session_destroy();

setcookie('user_id', '', -1);
setcookie('username', '', -1);
$home_url = 'http://' . $_SERVER['HTTP_HOST'] . '/test/log.php';
header('Location: ' . $home_url);
