<?php
  session_start();
  require_once('db/db.php');
?>

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title></title>
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style_1.css">
</head>
<?php
  if(!empty($_COOKIE['username'])){
?>
<body>
  <div class="form">

    <?php

      $name = $_COOKIE['username'];

      $sql2 = "SELECT * FROM `signup` WHERE login = '$name' OR email = '$name'";
      $rs2 = mysqli_query($conn,  $sql2);

      while($row2 = mysqli_fetch_assoc($rs2)) {
          $data2 = $row2;
          echo $date_today = date("m.d.y");
      ?>
      <span style=" color: #a0b3b0; text-align: center; font-size: 18px; "> Hello, <?php echo $data2['user_name']; ?> </span>

    <br>

    <?php
      }
    ?>

    <div class="tab-group" style="margin-top:13px;">
      <a href="exit.php">Exit</a>

    <?php
      if ($_SESSION) {
    ?>
        <a href="admin/index.php">Admin</a>
    <?php
      }else{
    ?>

        <button class="show_popup button button-block" type="button" rel="popup" style="width:50%; height: 58px;">Admin panel</button>
    <?php
      }

       $sql = "SELECT * FROM `page`";
  	   $rs = mysqli_query($conn,  $sql);

  		 while($row = mysqli_fetch_assoc($rs)) {
          	$data = $row;
    ?>
      <a href="page.php/?id=<?php echo $data['id'] ?>" style="width: 15%; margin-top:2px;margin-right:2px;"><?php echo $data['title'] ?></a>
    <?php
      }
    ?>
    </div>
  </div>
  <div class="overlay_popup" style="display: none;position: fixed; top:0;right: 0;left: 0;bottom: 0;background: #000;opacity: 0.3;"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="popup" id="popup" style="display:none;">
          <div class="object" style="margin: 0px 29% 9px 29%; height: 260px;">

            <form action="admin/admin_login.php" method="post" style=" height: 260px; ">
              <br>
              <p style="font-size: 20px; color: #a0b3b0;"><b>Админ панель</b></p>

              <div class="form-group">
                <input class="form-control" name="email" type="e-mail" size="35" placeholder="E-mail">
              </div>

              <div class="form-group">
                <input class="form-control" name="pass" type="password" size="35" placeholder="Пароль"><br>
              </div>

              <div class="pull-left">
                <input class="button button-block" type="submit" name="button" value="Вход" style="margin-left: 50%; width: 170%;">

              </div>

            </form>

          </div>
        </div>
      </div>
      <div class="col-md-3"></div>
    </div>
  </div>
<?php
  $sql1 = "SELECT * FROM `couriers`";
  $rs1 = mysqli_query($conn,  $sql1);

  while($row1 = mysqli_fetch_assoc($rs1)) {
       $data1 = $row1;
?>

  <div class="container">
    <div class="form">
      <div class="row">
        <div class="col-sm-3">
          <div style="text-align:center; margin-bottom:15px; color: #a0b3b0;font-size: 20px;"><?php echo $data1['name'] ?></div>
          <div style="text-align:center; margin-bottom:15px; color: #a0b3b0;font-size: 20px;"><?php echo $data1['destination'] ?></div>
        </div>
        <div class="col-sm-3">
          <div style="text-align:center; margin-bottom:15px; color: #a0b3b0;font-size: 20px;"><?php echo $data1['startT'] ?></div>
          <div style="text-align:center; margin-bottom:15px; color: #a0b3b0;font-size: 20px;"><?php echo $data1['endT'] ?></div>
        </div>
        <div class="col-sm-3">
          <div style="text-align:center; margin-bottom:15px; color: #a0b3b0;font-size: 20px;"><?php echo $data1['startD'] ?></div>
          <div style="text-align:center; margin-bottom:15px; color: #a0b3b0;font-size: 20px;"><?php echo $data1['endD'] ?></div>
        </div>
        <?php
        if ($_SESSION) {
        ?>
        <div class="col-sm-3">
          <div class="tab-group">
            <a href="delete_couriers.php?id=<?php echo $data1['id'] ?>" style="width:100%">Delete</a>
          </div>
        </div>
      <?php } ?>
      </div>
    </div>
  </div>

<?php } ?>



</body>
<?php
  }else{
    $home_url = 'http://' . $_SERVER['HTTP_HOST'] . '/test/log.php';
    header('Location: ' . $home_url);
  }
?>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  <script type="text/javascript">
    $('.show_popup').click(function() {
    var popup_id = $('#' + $(this).attr("rel"));
    $(popup_id).show();
    $('.overlay_popup').show();
  });
    $('.overlay_popup').click(function() {
    $('.overlay_popup, .popup').hide();
  });
  </script>

  <script  src="js/index.js"></script>

</html>
